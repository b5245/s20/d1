


// [SECTION]	JSON Object
/*
	- JSON stands for JavaScript Object Notation
	- A common use of JSON is to read data from web server, and display the data within a webpage.

	Syntax:
	{
		"propertyA": "valueA",
		"propertyB": "valueB",

	}
*/

// JSON Object

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila":
// 	"country": "Philippines",

// }


// // [SECTION]	JSON Array

// "cities": [
// 	{"city": "Quezon City", "province": "Metro Manila":"country": "Philippines"}
// 	{"city": "Antipolo City", "province": "Metro Manila":"country": "Philippines"}
// 	{"city": "Marikina City", "province": "Metro Manila":"country": "Philippines"}

// ]

// [SECTION] JSON Methods
/*
	JSON.stringify - converts JavaScript Object/Array into string.
	JSON.parse - converts JSON format to Javascript.
*/

let batchesArr = [
	{batchName: 'Batch 177'},
	{batchName: 'Batch 178'},
	{batchName: 'Batch 179'}
];

console.log(batchesArr);
let batchArrCont = JSON.stringify(batchesArr);
console.log(`Result from stringify method: `);
console.log(batchArrCont);

// parsing before containing to variable
let data = JSON.stringify({
	name: `John`,
	age: 31,
	address: {
		city: `Manila`,
		country: `Philippines`
	}
});

console.log(data)

// JSON.parse
console.log(`Result from parse method:`);
let dataParse = JSON.parse(data)
console.log(dataParse);
console.log(dataParse.name);

console.log(JSON.parse(batchArrCont));